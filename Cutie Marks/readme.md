This sub-folder includes all cutie marks I have done for any purpose – new pony, curiosity, gift for someone else, the likes – arranged according to their holders' roles in the show. Most of these are what would be seen if the pony is facing right, but there are notable exceptions:

* The mane six
* Shining Armour
* Derpy Hooves
* Big Macintosh
* Say Cheese (optimised from original by Ambassad0r)

The colours are [MLP Vector Club recommendations](https://mlpvc-rr.ml/cg), with missing values taken from [here](https://i.imgur.com/bHwUS7R.png) and canon screenncaps. Some cutie marks may not appear as intended in other SVG viewers like web browsers due to my practice of leaving out XML declarations, but all should open correctly in Inkscape, which puts them in when needed.
